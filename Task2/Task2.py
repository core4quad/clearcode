def damage(spell):
    """spell:STRING
    Function calculating damage from input spell.
    Function return a value of damage."""

    left=None #spell start
    right=None #spell end
    weight=[ 'dai',5, 'aine',4, 'ain',3, 'ai',2, 'ne',2, 'jee',3, 'je',2 ] #points of damage

    ###Left cutting:
    for i in range(0,len(spell)-3):
        if((spell[i]+spell[i+1])=="fe"):
            left=i+2
            break
        
    if (left==None): #no start
        return 0

    ###Right cutting:
    for i in range(len(spell)-1,left,-1):
        if((spell[i-1]+spell[i])=="ai"):
            right=i-1
            break

    if (right==None): #no end
        return 0;

    ###Check more than one 'fe' break:
    for i in range(left,right-1):
        if ((spell[i]+spell[i+1])=='fe'):
            return 0;

    ###Calculating spell:
    ret=3-(right-left) #damage of spell witchout any subspell

    for w in range(0,len(weight),2):
        for i in range(left,right-len(weight[w])+1):
            temp=spell[i:i+len(weight[w])] #subspell what we check
            if(temp==weight[w]):
                mask="" #if subspell found- create mask
                for j in range(0,len(weight[w])):
                    mask+="X"
                spell=str(spell[:i])+str(mask)+str(spell[i+len(weight[w]):]) #update spell
                ret=ret+len(weight[w])+weight[w+1] #update damage

    if (ret>0):
        return ret;

    else:
        return 0;


#TESTS:
a1=damage('feeai')
print(a1)
a2=damage('feaineain')
print(a2)
a3=damage('jee')
print(a3)
a4=damage('fefefefefeaiaiaiaiai')
print(a4)
a5=damage('fdafafeajain')
print(a5)
a6=damage('fexxxxxxxxxxai')
print(a6)

