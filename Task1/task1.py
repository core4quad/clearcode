def group_by( stream, field, success=None ):
    """stream=STREAM; field='year'/'month'; success='None'/'True'/'False'
    Function group log with orbital launches.
    Function return a dict with years/months and success/failure/all rockets launch."""

    ###Opening file:
    try:
        text=stream.read()
    except:
        raise IOError ("No such file!")

    ###Few checks:
    if ( field!='year' and field != 'month' ):
        raise ValueError ("Bad field inserted!")
    if ( success != None and success != True and success != False):
        raise ValueError ("Bad success inserted!")
    ret={} #dict answer
    i=401 #header offset

    ###Main loop:
    while ( i < len(text)-1 ):
        i+=1
        if ( field == 'year' ):
            temp_date = text[i] + text[i+1] + text[i+2] + text[i+3] #save year
        else:
            temp_date = text[i+18] + text[i+19] + text[i+20] #save month

        if ( text[i] == " " ): #sub-rockets:
            if ( success == None ):
                ret[temp_date_backup[0]]+=1

            elif ( success == True and temp_date_backup[1] == "S"):
                ret[temp_date_backup[0]]+=1

            elif ( success == False and temp_date_backup[1] == "F"):
                ret[temp_date_backup[0]]+=1     
        
        else: #main-rockets:
            if ( success == None ):
                try:
                    ret[temp_date]+=1
                except:
                    ret[temp_date]=1

            elif ( success == True and text[i+193] == 'S' ):
                try:
                    ret[temp_date]+=1
                except:
                    ret[temp_date]=1

            elif ( success == False and text[i+193] == 'F' ):
                try:
                    ret[temp_date]+=1
                except:
                    ret[temp_date]=1

            temp_date_backup=(temp_date,text[i+193]) #save main-rocket

        #go to the next line:
        while ( ord(text[i]) != 10 ): 
            i=i+1
            
    stream.close()
    return ret




#TESTS:
a1=group_by(open("launchlog.txt"),'year')
print(a1)
print("")
a2=group_by(open("launchlog.txt"),'month')
print(a2)
